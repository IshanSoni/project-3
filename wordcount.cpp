/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: http://www.cplusplus.com/reference/string/string/
 * http://www.cplusplus.com/reference/set/set/
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: ~5
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;

unsigned long countWords(const string& s, set<string>& wl)
{
   int start = 0;
   int wordCount = 0;
   
   int j = 0;
   while(s[0] == ' ' && j < s.length())         // detects if the string input starts with whitespace    
   {
      if(s[0] == ' ' && s.at(j) == ' ' && s.at(j+1) != ' ')
	  {
	     start = j+1;
		 break;
	  }
	  j++;
   }
   
   for (int i = start; i < s.length(); i++)    // adds each word in the string to a set and counts # of words
   {  
	  if(i == s.length()-1 || ((s.at(i) == ' ') && (s.at(i+1) != ' ')) 
	     || (s.at(i) == '\t' && s.at(i+1) != '\t')) 
	  {
         if(i == s.length()-1)
		    i++;  	  
		 wl.insert(s.substr(start, i-start));
		 start = i+1;
		 wordCount++;
      }		  
   }
   return wordCount;                          //returns the number of words in the string
}

int main()
{
	string line;
	set<string> wordList;
	set<string> lineList;
	int numberOfLines = 0;
	int numberOfWords = 0;
	int numberOfChars = 0;
	
	while(getline(cin, line))
	{
	   numberOfWords += countWords(line, wordList);   //computes # of words by calling the countWords function  
	   numberOfLines++;	                              //computes # of lines by incrementing everytime input is called
	   numberOfChars++;                               //adds 1 to # of characters each time input is called to account for \n character
	   numberOfChars += line.length();                //computers # of characters by finding the size of the input
	   lineList.insert(line);                         //adds each input to a set of string to store the lines
	}
	
	cout << numberOfLines << "\t" << numberOfWords << "\t" 
	     << numberOfChars << "\t" << lineList.size() << "\t" //unique lines are computed by finding the size of the set of lines
		 << wordList.size() << endl;                         //unique words are computed by finding the size of the set of words
	
	return 0;
}
